//Message 
var mongoose  = require('mongoose');
var moment    = require('moment');
var paginator = require('mongoose-paginate');

var Schema = mongoose.Schema;

var SubscribeSchema = new Schema({
  plan_id:               { type: Schema.Types.ObjectId, ref: 'Plan' },
  user_id:               { type: Schema.Types.ObjectId, ref: 'User' },
  isActive:              { type: Boolean, default: true}, 
  start_date:            { type: Date},
  end_date:              { type: Date},
  date_created:          { type: Date},
  last_modified:         { type: Date}
});

// add mongoose-troop middleware to support pagination
SubscribeSchema.plugin(paginator);

/**
 * Pre save middleware.
 *
 * @desc  - Sets the date_created and last_modified
 *          attributes prior to save.
 *        - Hash tokens password.
 */
SubscribeSchema.pre('save', function preSaveMiddleware(next) {
  var token = this;

  // set date modifications
  var now = moment().toISOString();

  token.date_created = now;
  token.last_modified = now;

  next();

});

/**
 * Subscribe Attributes to expose
 */
SubscribeSchema.statics.whitelist = {
  _id:           1,
  plan_id:       1,
  user_id:       1,
  isActive:      1, 
  start_date:    1,
  end_date:      1,
  date_created:  1,
  last_modified: 1
};


// Expose Subscribe Model
module.exports = mongoose.model('Subscribe', SubscribeSchema);
