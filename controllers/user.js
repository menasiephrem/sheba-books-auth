/**
 * Load Module Dependencies
 */

var EventEmitter    = require('events').EventEmitter;
var debug 			= require('debug')('api:user-controller');
var moment    		= require('moment'); 
var sgMail			= require('@sendgrid/mail');

var UserDal  		= require('../dal/user');
var config			= require('../config');


/**
 * Create User
 * 
 * @desc Create User and User Type then Respond with
 *       created user
 * 
 * @throws {USER_CREATION_ERROR}
 *
 * @param {Object} req HTTP Request Object
 * @param {Object} res HTTP Response Object
 * @param {Function} next Middleware Dispatcher
 */
exports.createUser = function createUser(req, res, next){
	debug('Create User');
	var body = req.body;

	var workflow = new EventEmitter();

	// Validate User Data
	// Trust No One
	workflow.on('validation', function () {
		req.checkBody('email')
		   .notEmpty().withMessage('Email is Empty')
		   .isEmail().withMessage('Email is not Valid');
		req.checkBody('password')
			.notEmpty().withMessage('Password is Empty');

		var errors = req.validationErrors();

		if(errors) {
			res.status(400);
			res.json({
				status: 400,
				type: 'USER_CREATION_ERROR',
				message: errors
			});
		} else {
			body.username = body.email;
			workflow.emit('exists');
		}
	});

	workflow.on('exists', function () {
		UserDal.get({ username: body.username }, function userExists(err, user){
			if(err) {
				res.status(500);
				res.json({
					status: 500,
					type: 'USER_CREATION_ERROR',
					message: err.message
				});
				return;
			}
			// truthy vs falsey
			if(user._id) {
				res.status(400);
				res.json({
					status: 400,
					type: 'USER_CREATION_ERROR',
					message: 'User With Those Credentials Exists'
				});
				return;
			}

			workflow.emit('createUser');
		})
	});

	workflow.on('createUser', function(){
		UserDal.create(body, function createUser(err, user){
			if(err) {
				res.status(500);
				res.json({
					status: 500,
					type: 'USER_CREATION_ERROR',
					message: err.message
				});
				return;
			}

			body.user = user._id;

			workflow.emit('sendEmail', user);
		});
	});

	workflow.on('sendEmail', function(user){
		sgMail.setApiKey(config.SENDGRID_API_KEY);
		const msg = {
		  to: user.username,
		  from: 'info@shebabooks.com',
		  subject: 'Thank you for signing up',
		  text: 'We are excited that you are joing us here at shebabooks',
		  html: '<strong>Lets have fun reading!!!</strong>',
		};
		sgMail.send(msg);
		workflow.emit('respond', user);
	});	

	workflow.on('respond', function(user) {
		res.status(201);
		res.json(user);
	});


	workflow.emit('validation');
};

/**
 * GET all Users
 * 
 * @desc Get all users using pagination the parmetrs are pass as via the url 
 *
 * @param {Object} req HTTP Request Object
 * @param {Object} res HTTP Response Object
 * @param {Function} next Middleware Dispatcher
 */
exports.getCollectionByPagination = function getCollectionByPagination(req, res, next){
	debug('Get Users Collection By Pagination');

	var query = req.query.query || {};
	var qs = req.query;

	UserDal.getCollectionByPagination(query, qs, function (err, docs){
		if(err) {
				res.status(500);
				res.json({
					status: 500,
					type: 'USER_COLLECTION_PAGINATED_ERROR',
					message: err.message
				});
				return;
		}

		res.json(docs);
	})
}

/**
 * Delete User
 * 
 * @desc Delete User and Corresponding User from the DB
 *
 * @param {Object} req HTTP Request Object
 * @param {Object} res HTTP Response Object
 * @param {Function} next Middleware Dispatcher
 */
 exports.remove = function removeUser(req, res, next) {
 	debug('Remove User: ', req.params.id);

 	var query = {
 		_id: req.params.id
 	};
 	
 	UserDal.delete(query, function deleteUser(err, user){
 		if(err){
 		  res.status(500);
      res.json({
        status: 500,
        type: 'USER_REMOVE_ERROR',
        message: err.message
      });
      return;
 		}

    if(!user._id) {
      res.status(400);
      res.json({
          status: 400,
          type: 'USER_REMOVE_ERROR',
          message: 'User Does not Exist!!'
      });
      return;
    }
    		res.json(user ||{});
 	})
 };


/**
 * GET a User
 * 
 * @desc Get a single user
 *
 * @param {Object} req HTTP Request Object
 * @param {Object} res HTTP Response Object
 * @param {Function} next Middleware Dispatcher
 */
exports.getUser = function getUser(req, res, next){
	var Id = req.params.id;
	UserDal.get({_id: Id}, function getCB(err, User){
		if(err){
			res.status(500);
			res.json({
				status: 500,
				type: 'GET_USER_ERROR',
				message: err.message
			});
			return;
		}
		res.json(User ||{});
	});

};

/**
 * Update a User
 * 
 * @desc  Update a single user
 *
 * @param {Object} req HTTP Request Object
 * @param {Object} res HTTP Response Object
 * @param {Function} next Middleware Dispatcher
 */
exports.updateUser = function updateUser(req, res, next){
	
	var Id = req.params.id;
	var body = req.body;
	var now = moment().toISOString();
	body.last_modified = now
	if(body.password){
			res.status(403);
			res.json({
				status: 403,
				type: 'UPDATE_USER_ERROR',
				message: "You can't update password here"
			});
			return;
	}

	UserDal.update({_id : Id}, body, function updateCB(err, User){

		if(err){
			res.status(500);
			res.json({
				status: 500,
				type: 'UPDATE_USER_ERROR',
				message: err.message
			});
			return;
		}
		res.json(User || {} );
	});
};

/**
 * Update a User's password
 * 
 * @desc  Update a single user's password
 *
 * @param {Object} req HTTP Request Object
 * @param {Object} res HTTP Response Object
 * @param {Function} next Middleware Dispatcher
 */
exports.updatePassword = function updatePassword(req, res, next){
	
	var Id 	 = req.params.id;
	var rawPassword  = req.body.password;
	var password = {};
	
	var workflow = new EventEmitter();

	workflow.on('hash_pw', function(){
		
		UserDal.hashPasswd(rawPassword, function updateCB(err, hash){

			if(err){
				res.status(500);
				res.json({
					status: 500,
					type: 'ERROR_ON_PASSWORD_HASH',
					
				});
				return;
			}
			var data = {
				user_id: Id,
				password: hash
			}
			workflow.emit('change_pw', data);
			
		});

	});

	workflow.on('change_pw', function(data){
		var body = {}

	var now = moment().toISOString();
	body.last_modified = now
	body.password =data.password;

		UserDal.update({_id : data.user_id}, body, function updateCB(err, User){

			if(err){
				res.status(500);
				res.json({
					status: 500,
					type: 'UPDATE_USER_PASSWORD_ERROR',
					message: err.message
				});
				return;
			}
			res.json(User || {} );
		});

	});

	workflow.emit('hash_pw');
};

// Noop Method
exports.noop = function noop(req, res, next) {
	res.json({
		message: 'To be Implemented'
	});
}