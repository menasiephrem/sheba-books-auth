/**
 * Load Module Dependencies
 */

var EventEmitter    = require('events').EventEmitter;
var debug 			= require('debug')('api:plan-controller');
var moment    		= require('moment');

var PlanDal  		= require('../dal/plan');


/**
 * Create Plan
 * 
 * @desc Create Plan and Plan Type then Respond with
 *       created Plan
 * 
 * @throws {Plan_CREATION_ERROR}
 *
 * @param {Object} req HTTP Request Object
 * @param {Object} res HTTP Response Object
 * @param {Function} next Middleware Dispatcher
 */
// POST Plans
exports.creatPlan = function creatPlan(req, res, next){
	debug('create plan');
	var body        = req.body;
	

	PlanDal.create(body, function createCB(err, Plan){
		if(err){
			res.status(500);
			res.json({
				status: 500,
				type: 'CREATE_Plan_ERROR',
				message: err.message
			});
			return;
		}
		res.status(201);
		res.json(Plan);
	});
};

/**
 * GET all Plans
 * 
 * @desc Get all Plans using pagination the parmetrs are pass as via the url 
 *
 * @param {Object} req HTTP Request Object
 * @param {Object} res HTTP Response Object
 * @param {Function} next Middleware Dispatcher
 */
exports.getCollectionByPagination = function getCollectionByPagination(req, res, next){
	debug('Get Plans Collection By Pagination');

	var query = req.query.query || {};
	var qs = req.query;

	PlanDal.getCollectionByPagination(query, qs, function (err, docs){
		if(err) {
				res.status(500);
				res.json({
					status: 500,
					type: 'PLAN_COLLECTION_PAGINATED_ERROR',
					message: err.message
				});
				return;
		}

		res.json(docs);
	})
}

/**
 * Delete Plan
 * 
 * @desc Delete Plan and Corresponding Plan from the DB
 *
 * @param {Object} req HTTP Request Object
 * @param {Object} res HTTP Response Object
 * @param {Function} next Middleware Dispatcher
 */
 exports.remove = function removePlan(req, res, next) {
 	debug('Remove Plan: ', req.params.id);

 	var query = {
 		_id: req.params.id
 	};
 	
 	PlanDal.delete(query, function deletePlan(err, Plan){
 		if(err){
 		  res.status(500);
      res.json({
        status: 500,
        type: 'Plan_REMOVE_ERROR',
        message: err.message
      });
      return;
 		}

    if(!Plan._id) {
      res.status(400);
      res.json({
          status: 400,
          type: 'Plan_REMOVE_ERROR',
          message: 'Plan Does not Exist!!'
      });
      return;
    }
    		res.json(Plan ||{});
 	})
 };


/**
 * GET a Plan
 * 
 * @desc Get a single Plan
 *
 * @param {Object} req HTTP Request Object
 * @param {Object} res HTTP Response Object
 * @param {Function} next Middleware Dispatcher
 */
exports.getPlan = function getPlan(req, res, next){
	var Id = req.params.id;
	PlanDal.get({_id: Id}, function getCB(err, Plan){
		if(err){
			res.status(500);
			res.json({
				status: 500,
				type: 'GET_Plan_ERROR',
				message: err.message
			});
			return;
		}
		res.json(Plan ||{});
	});

};

/**
 * Update a Plan
 * 
 * @desc  Update a single Plan
 *
 * @param {Object} req HTTP Request Object
 * @param {Object} res HTTP Response Object
 * @param {Function} next Middleware Dispatcher
 */
exports.updatePlan = function updatePlan(req, res, next){
	
	var Id = req.params.id;
	var body = req.body;
	var now = moment().toISOString();
	body.last_modified = now
	PlanDal.update({_id : Id}, body, function updateCB(err, Plan){

		if(err){
			res.status(500);
			res.json({
				status: 500,
				type: 'UPDATE_Plan_ERROR',
				message: err.message
			});
			return;
		}
		res.json(Plan || {} );
	});
};

// Noop Method
exports.noop = function noop(req, res, next) {
	res.json({
		message: 'To be Implemented'
	});
}