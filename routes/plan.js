// Load Module Dependencies
var express = require('express');

var planController = require('../controllers/plan');

var router = express.Router();

/**
 * @api {POST} /plans/ Create Plan
 * @apiName CreatPlan
 * @apiGroup Plan
 *
 * @apiDescription Creates a new Plan.
  * @apiHeader  {String} Authorization  Bearer + {token} a required header for all the API end-points. The token can be found form the login end point
* 
* @apiHeaderExample {json} Header-Example:
*     {
*       "Authorization": "Bearer HrAuoeKL2NtpG9tRKkZ/J0vjDZu1psZneK8bKpdO6Ps8x5BXFsA9oMnSD4GCnxk9oWD2u3cmokj7"
*     }
 *
 * @apiParam {String} name the name  of  Plan to be created
 * @apiParam {String} description the description the plan to be created

 * @apiParamExample Request Example:
{
	"name":			"Legend",
	"description":	"A Legend plan to subscribe"
}
 *
 *
 * @apiSuccessExample Response Example:
 *  HTTP/1.1 201 Created
{
    "_id": "5a7eb05429edd235c2e6edf5",
    "last_modified": "2018-02-10T08:41:56.299Z",
    "date_created": "2018-02-10T08:41:56.299Z",
    "name": "Legend",
    "description": "A Legend plan to subscribe"
}
 */
router.post('/', planController.creatPlan);

/**
 * @api {GET} /plans/ Get Plan By Pagination
 * @apiName Getplans
 * @apiGroup Plan
 *
 * @apiDescription Get all plans by pagination

  * @apiHeader  {String} Authorization  Bearer + {token} a required header for all the API end-points. The token can be found form the login end point
* 
* @apiHeaderExample {json} Header-Example:
*     {
*       "Authorization": "Bearer HrAuoeKL2NtpG9tRKkZ/J0vjDZu1psZneK8bKpdO6Ps8x5BXFsA9oMnSD4GCnxk9oWD2u3cmokj7"
*     }
 *
 * @apiSuccessExample Response Example:
 *  HTTP/1.1 200 OK
 {
    "page": 1,
    "total_docs": 3,
    "total_pages": 1,
    "per_page": 10,
    "docs": [
        {
            "_id": "5a7eaa8408644b31db10f108",
            "last_modified": "2018-02-10T08:36:29.384Z",
            "date_created": "2018-02-10T08:17:08.499Z",
            "name": "Economy",
            "__v": 0,
            "description": "An Economy plan to subscribe"
        },
        {
            "_id": "5a7eaaa708644b31db10f109",
            "last_modified": "2018-02-10T08:17:43.338Z",
            "date_created": "2018-02-10T08:17:43.338Z",
            "name": "Starter",
            "description": "A starter plan to subscribe",
            "__v": 0
        },
        {
            "_id": "5a7eb05429edd235c2e6edf5",
            "last_modified": "2018-02-10T08:41:56.299Z",
            "date_created": "2018-02-10T08:41:56.299Z",
            "name": "Legend",
            "description": "A Legend plan to subscribe",
            "__v": 0
        }
    ]
}

 */
router.get('/', planController.getCollectionByPagination);


/**
 * @api {GET} /plans/ Get a plan
 * @apiName GetPlane
 * @apiGroup Plan
 *
 * @apiDescription Get a single plan

  * @apiHeader  {String} Authorization  Bearer + {token} a required header for all the API end-points. The token can be found form the login end point
* 
* @apiHeaderExample {json} Header-Example:
*     {
*       "Authorization": "Bearer HrAuoeKL2NtpG9tRKkZ/J0vjDZu1psZneK8bKpdO6Ps8x5BXFsA9oMnSD4GCnxk9oWD2u3cmokj7"
*     }
 *
 * @apiSuccessExample Response Example:
 *  HTTP/1.1 200 OK
 {
    "_id": "5a7eaa8408644b31db10f108",
    "last_modified": "2018-02-10T08:36:29.384Z",
    "date_created": "2018-02-10T08:17:08.499Z",
    "name": "Economy",
    "description": "An Economy plan to subscribe"
}
 */
router.get('/:id',planController.getPlan);

/**
 * @api {PUT} /plans/:id Update a Plan
 * @apiName UpdatePlan
 * @apiGroup Plan
 *
 * @apiDescription Update a single plan

  * @apiHeader  {String} Authorization  Bearer + {token} a required header for all the API end-points. The token can be found form the login end point
* 
* @apiHeaderExample {json} Header-Example:
*     {
*       "Authorization": "Bearer HrAuoeKL2NtpG9tRKkZ/J0vjDZu1psZneK8bKpdO6Ps8x5BXFsA9oMnSD4GCnxk9oWD2u3cmokj7"
*     }
 *
 * @apiParam {String} name the name  of  Plan to be updated
 * @apiParam {String} description the description the plan to be updated
 * @apiParamExample Request Example:

{
	"name":			"Economy",
	"description":	"A Economy plan to subscribe"
}

 * @apiSuccessExample Response Example:
 *  HTTP/1.1 200 OK
 {
    "_id": "5a7eaa8408644b31db10f108",
    "last_modified": "2018-02-10T08:56:07.177Z",
    "date_created": "2018-02-10T08:17:08.499Z",
    "name": "Economy",
    "description": "An Economy plan to subscribe"
}

 */
router.put('/:id',planController.updatePlan);

/**
 * @api {DELETE} /plans/:id Delete a Single Plan
 * @apiName DeletePlan
 * @apiGroup Plan
 *
 * @apiDescription Delete a single Plan

  * @apiHeader  {String} Authorization  Bearer + {token} a required header for all the API end-points. The token can be found form the login end point
* 
* @apiHeaderExample {json} Header-Example:
*     {
*       "Authorization": "Bearer HrAuoeKL2NtpG9tRKkZ/J0vjDZu1psZneK8bKpdO6Ps8x5BXFsA9oMnSD4GCnxk9oWD2u3cmokj7"
*     }
 *
 * @apiSuccessExample Response Example:
 *  HTTP/1.1 200 OK
 {
    "_id": "5a7eaa8408644b31db10f108",
    "last_modified": "2018-02-10T08:56:07.177Z",
    "date_created": "2018-02-10T08:17:08.499Z",
    "name": "Economy",
    "description": "An Economy plan to subscribe"
}
 */
router.delete('/:id', planController.remove);

module.exports = router;