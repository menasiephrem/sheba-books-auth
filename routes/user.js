// Load Module Dependencies
var express = require('express');

var userController = require('../controllers/user');
var authController = require('../controllers/auth');

var router = express.Router();


/*
 * @api {POST} /users/signup User Signup
 * @apiName SignupUser
 * @apiGroup User
 *
 * @apiDescription Creates a new User.
 *
 * @apiParam {String} email User Email
 * @apiParam {String} password User Password
 * @apiParamExample Request Example:
 {
	"email": "mao@sheba.com",
 	"password": "123456"
 }
 * @apiSuccess {String} _id User Id
   @apiSuccess {Date}  last_modified last date modified
   @apiSuccess {Date}  date_created last date modified
 * @apiSuccess {String} username User Username
 * @apiSuccess {Boolean} isActive Is the user Active
 *
 * @apiSuccessExample Response Example:
 *  HTTP/1.1 201 Created
{
    "_id": "5a7e8a51bc157618d44d8278",
    "last_modified": "2018-02-10T05:59:45.822Z",
    "date_created": "2018-02-10T05:59:45.822Z",
    "username": "mao@sheba.com",
    "isActive": true
}
*/
router.post('/signup', userController.createUser);

/**
 * @api {POST} /users/login User Login
 * @apiName LoginUser
 * @apiGroup User
 *
 * @apiDescription Creates a new User and corresponding Profile.
 *
 * @apiParam {String} email User username
 * @apiParam {String} password User Password
 * @apiParamExample Request Example:
  {
 	"username": "mao@gebeya.com",
 	"password": "123456"
   }
 *
 * @apiSuccess {String} tokne User token
 * @apiSuccess {User} User
 *
 * @apiSuccessExample Response Example:
 *  HTTP/1.1 201 Created
    {
    "token": {
        "_id": "5a7e8c00d07a671a0c9ef414",
        "date_created": "2018-02-10T06:06:56.124Z",
        "value": "gPzBV63Z9H6eIiIzNvPnUbh1QN4z+saIX2oNgsa3J/FAxmLFgKFgTkcf7cvSuT5GcfytrCxdqkK4",
        "user": {
            "_id": "5a7e8a51bc157618d44d8278",
            "last_modified": "2018-02-10T05:59:45.822Z",
            "date_created": "2018-02-10T05:59:45.822Z",
            "username": "mao@sheba.com",
            "isActive": true
        },
        "revoked": false
    }
}
*/
router.post('/login', authController.loginUser);

/**
 * @api {PUT} /users/logout User Logout
 * @apiName LogoutUser
 * @apiGroup User
 *
 * @apiDescription logouts out the login user.
  * @apiHeader  {String} Authorization  Bearer + {token} a required header for all the API end-points. The token can be found form the login end point
* 
* @apiHeaderExample {json} Header-Example:
*     {
*       "Authorization": "Bearer gPzBV63Z9H6eIiIzNvPnUbh1QN4z+saIX2oNgsa3J/FAxmLFgKFgTkcf7cvSuT5GcfytrCxdqkK4"
*     }
  * @apiSuccessExample Response Example:
 *  HTTP/1.1 201 Created
 {
    "logged_out": true
}
 */
router.put('/logout', authController.logoutUser);


/**
 * @api {GET} /users/ Get all users
 * @apiName GetAllUsers
 * @apiGroup User
 *
 * @apiDescription Get all users using pagination
 * @apiHeader  {String} Authorization  Bearer + {token} a required header for all the API end-points. The token can be found form the login end point
 * @apiSuccessExample Response Example:
 *  HTTP/1.1 200 OK
 {
    "page": 1,
    "total_docs": 2,
    "total_pages": 1,
    "per_page": 10,
    "docs": [
        {
            "_id": "5a7e869e8b822916071d7ce6",
            "last_modified": "2018-02-10T05:43:59.052Z",
            "date_created": "2018-02-10T05:43:59.052Z",
            "password": "$2a$10$SM2GWZMqmW8UTZ3PJHcOJOY6LBVYu7cjNC3nY.H1nJJhoN8zAq9o6",
            "username": "mamo@sheba.com",
            "__v": 0,
            "role": "client",
            "isActive": false
        },
        {
            "_id": "5a7e8a51bc157618d44d8278",
            "last_modified": "2018-02-10T05:59:45.822Z",
            "date_created": "2018-02-10T05:59:45.822Z",
            "password": "$2a$10$FjgniSRH/ZN/tTDR9OvZWO4SGp7sTVvZi7pRG7RbsbwJr/a4JMkaS",
            "username": "mao@sheba.com",
            "__v": 0,
            "isActive": true
        }
    ]
}
*/
router.get('/',userController.getCollectionByPagination);

/**
 * @api {GET} /users/:id Get a single user
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiDescription Get a single user
 * @apiHeader  {String} Authorization  Bearer + {token} a required header for all the API end-points. The token can be found form the login end point
 * @apiSuccessExample Response Example:
 *  HTTP/1.1 200 OK
 {
            "_id": "5a7e8a51bc157618d44d8278",
            "last_modified": "2018-02-10T05:59:45.822Z",
            "date_created": "2018-02-10T05:59:45.822Z",
            "password": "$2a$10$FjgniSRH/ZN/tTDR9OvZWO4SGp7sTVvZi7pRG7RbsbwJr/a4JMkaS",
            "username": "mao@sheba.com",
            "__v": 0,
            "isActive": true
        }

 */
router.get('/:id', userController.getUser);

/**
 * @api {PUT} /users/:id Update a User
 * @apiName UpdateUser
 * @apiGroup User
 *
 * @apiDescription Update a single User, you can pass all information on the user model except password, you can't change password using this end point.

 * @apiHeader  {String} Authorization  Bearer + {token} a required header for all the API end-points. The token can be found form the login end point
* 
* @apiHeaderExample {json} Header-Example:
*     {
*       "Authorization": "Bearer HrAuoeKL2NtpG9tRKkZ/J0vjDZu1psZneK8bKpdO6Ps8x5BXFsA9oMnSD4GCnxk9oWD2u3cmokj7"
*     }
 *
 * @apiParam {String} [role] User role
 * @apiParam {String} [phone_number] User emailst_name
 * @apiParamExample Request Example:

{

  "role":           "client",
  "isActive":      false
}

 * @apiSuccessExample Response Example:
 *  HTTP/1.1 200 OK
 {
    "_id": "5a7e869e8b822916071d7ce6",
    "last_modified": "2018-02-10T05:43:59.052Z",
    "date_created": "2018-02-10T05:43:59.052Z",
    "username": "mamo@sheba.com",
    "role": "client",
    "isActive": false
}
 */
router.put('/:id', userController.updateUser);
/**
 * @api {DELETE} /users/:id Delete a single user
 * @apiName DeleteUser
 * @apiGroup User
 *
 * @apiDescription Delete a single user
 * @apiHeader  {String} Authorization  Bearer + {token} a required header for all the API end-points. The token can be found form the login end point
 * @apiSuccessExample Response Example:
 *  HTTP/1.1 200 OK
 {
            "_id": "5a7e8a51bc157618d44d8278",
            "last_modified": "2018-02-10T05:59:45.822Z",
            "date_created": "2018-02-10T05:59:45.822Z",
            "password": "$2a$10$FjgniSRH/ZN/tTDR9OvZWO4SGp7sTVvZi7pRG7RbsbwJr/a4JMkaS",
            "username": "mao@sheba.com",
            "__v": 0,
            "isActive": true
        }

 */
router.delete('/:id', userController.remove);

/**
 * @api {PUT} /users/:id Update a User's password
 * @apiName UpdateUserPassword
 * @apiGroup User
 *
 * @apiDescription Update a single User by passing the new password in the body
 * @apiHeader  {String} Authorization  Bearer + {token} a required header for all the API end-points. The token can be found form the login end point
* 
* @apiHeaderExample {json} Header-Example:
*     {
*       "Authorization": "Bearer HrAuoeKL2NtpG9tRKkZ/J0vjDZu1psZneK8bKpdO6Ps8x5BXFsA9oMnSD4GCnxk9oWD2u3cmokj7"
*     }
 *
 * @apiParam {String} password User's new password
 * @apiParamExample Request Example:
{

  "password":   "new_password",
}

 * @apiSuccessExample Response Example:
 *  HTTP/1.1 200 OK
 {
    "_id": "5a7e869e8b822916071d7ce6",
    "last_modified": "2018-02-10T05:43:59.052Z",
    "date_created": "2018-02-10T05:43:59.052Z",
    "username": "mamo@sheba.com",
    "role": "client",
    "isActive": false
}
 */
router.put('/:id/changePassword',userController.updatePassword); 

// Expose Router Interface
module.exports = router;