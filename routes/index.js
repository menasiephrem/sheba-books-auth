// Load Module Dependencies

var userRouter  	= require('./user');
var planRouter		= require('./plan');

module.exports = function appRouter(app) {
	// User Routes
	app.use('/users', userRouter);

	// Plan Routes
	app.use('/plans', planRouter);


	app.get('/', function getRoot(req, res){
		res.json({
			message: 'Sheba Books API auth service'
		})
	});

}