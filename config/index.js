// Load Module Dependencies

module.exports = {
	// HTTP PORT
	HTTP_PORT: process.env.PORT||4900,

	// DB Connection URL
	MONGODB_URL: 'mongodb://127.0.0.1:27017',

	TOKEN_LENGTH: 57,

	SENDGRID_API_KEY: process.env.SENDGRID_API_KEY|| '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',

	CORS_OPTS: {
		    origin: '*',
		    methods: 'GET,POST,PUT,DELETE,OPTIONS',
		    allowedHeaders: 'Origin,X-Requested-With,Content-Type,Accept,Authorization'
  	},
};