# Sheba Books: Auth-service 
-Menasi Ephrem menasiephrem@gmail.com

This is Sheba books auth-service API: it is responsible for the Manipulation of both User and Plan models. Authorization is also allowed through this service by allowing user to sign up, log in and log out.

The Api documentaion is built by apiDoc and can be accessed by using the following link [documentation link](https://menasiephrem.github.io/sheba/) 

Here is a link to postman collection that has all the end points [postman collection link](https://www.getpostman.com/collections/996d93dbfb71d44d0a9f)  

### Installation

Sheba Books:Auth-service requires [Node.js](https://nodejs.org/) v4+ to run.

Install the dependencies and devDependencies and start the server.

```sh
$ cd sheba-auth
$ npm install 
$ npm start
```

For testing...

```sh
$ npm test
```

Here is the link to the API baseurl (the NON-Docker deployment)  [https://sheba-auth.herokuapp.com/ ](https://sheba-auth.herokuapp.com/ )

A link to the API baseurl (the Docker deployment) [https://sheba-auth-docker.herokuapp.com/ ](https://sheba-auth-docker.herokuapp.com/ )



