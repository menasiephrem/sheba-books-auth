var chai 	= require('chai');
var request = require('supertest');
var chaiHttp = require('chai-http');
var app = require('../app');
var userFix = require('./fixtures/user');

chai.use(chaiHttp);

var expect = chai.expect;

var userId   = null;
var authToke = '';

describe('Root Page',function(){

	describe('GET /', function(){
		it('should display the home page', function(done){
			request(app)
			  .get('/')
		   	  .end(function(err,res){
			  	if(err){
			  		return done(err);
			  	}

			  	if(res.status !== 200){
			  		var _err = JSON.parse(res.text);
			  		return done(new Error(_err.message));
			  	}

			  	var user = res.body;

			  	expect(user.message).to.be.a('string');
			  	expect(user.message).to.equal('Sheba Books API auth service');
			  	done();
			  })
		})
	})
})


describe('User Signup', function(){
	describe('POST /users/signup', function(){
		it('should signup a new user', function(done){
			 request(app)
			  .post('/users/signup')
			  .send(userFix)
			  .set('Content-Type', 'application/json')
			  .end(function (err, res){
			  	if(err){
			  		return done(err);
			  	}

			  	if(res.status !== 201){
			  		var _err = JSON.parse(res.text);
			  		return done(new Error(_err.message));
			  	}

			  	var user = res.body;

			  	expect(user._id).to.be.a('string');
			  	expect(user.isActive).to.equal(true);
			  	expect(user.username).to.equal(userFix.email)
			  	userId = user._id;
			  	done();
			  });
		});
	});
});

describe('User Log in', function(){
	var loginInfo = {
		username : userFix.email,
		password : userFix.password
	};

	describe('POST /users/login', function(){
		it('should login a user', function(done){
			request(app)
			  .post('/users/login')
			  .send(loginInfo)
			  .set('Content-Type', 'application/json')
			  .end(function (err, res){
	            if(err){
	              return done(err);
	            }

	            if(res.status !== 200) {
	              var _err = JSON.parse(res.text);
	              return done(new Error(_err.message));
	            }

	            var data = res.body;
	            
	            expect(data.token._id).to.be.a('string');
	            expect(data.token.value).to.be.a('string')
	            expect(data.token.revoked).to.equal(false);

	            authToken = data.token.value;
	            userId = data.token.user;

	            done();


			  });
		});		
	});

});


describe('Get a Single User', function(){
	describe('GET /users/:id',function(){
		it('It should return one single user',function(done){
		  request(app)
		    .get('/users/'+userId)
		    .set('Content-Type', 'application/json')
		    .set('Authorization', 'Bearer ' + authToken)
		    .end(function(err, res){
			    if(err){
	              return done(err);
	            }

	            if(res.status !== 200) {
	              var _err = JSON.parse(res.text);
	              return done(new Error(_err.message));
	            }

	            var user = res.body;

	            expect(user._id).to.be.a('string');
	            expect(user._id).to.equal(userId);
	            expect(user.username).to.equal(userFix.email);

	            done();
		    });
		});
	});
});


describe('Get User Collection', function(){
	describe('GET /users/',function(){
		it('It should return collection of user by pagiantion',function(done){
		  request(app)
		    .get('/users/')
		    .set('Content-Type', 'application/json')
		    .set('Authorization', 'Bearer ' + authToken)
		    .end(function(err, res){
			    if(err){
	              return done(err);
	            }

	            if(res.status !== 200) {
	              var _err = JSON.parse(res.text);
	              return done(new Error(_err.message));
	            }

	            var data = res.body;

	            expect(data.page).to.be.a('number');
	            expect(data.total_docs).to.be.a('number');
	            expect(data.total_pages).to.be.a('number');
	            expect(data.per_page).to.be.a('number');
	            expect(data.page).to.be.a('number');
	            expect(data.docs).to.a('array');

	            done();
		    });
		});
	});
});

describe('User Update', function(){
	var updateInfo = {
  		"role":           "client",
  		"isActive":      false
	};

	describe('PUT /users/:id', function(){
		it('should update a user', function(done){
			request(app)
			  .put('/users/'+userId)
			  .send(updateInfo)
			  .set('Content-Type', 'application/json')
			  .set('Authorization', 'Bearer ' + authToken)
			  .end(function (err, res){
	            if(err){
	              return done(err);
	            }

	            if(res.status !== 200) {
	              var _err = JSON.parse(res.text);
	              return done(new Error(_err.message));
	            }

	            var data = res.body;
	            
	            expect(data.role).to.be.a('string');
	            expect(data.role).to.be.equal('client');
	            expect(data.isActive).to.equal(false);


	            done();


			  });
		});		
	});

});


describe('Change password', function(){
	var updateInfo = {
  		"password":   "new_password"
	};

	describe('PUT /users/:id/changePassword', function(){
		it('should change password of a user', function(done){
			request(app)
			  .put('/users/'+userId+'/changePassword')
			  .send(updateInfo)
			  .set('Content-Type', 'application/json')
			  .set('Authorization', 'Bearer ' + authToken)
			  .end(function (err, res){
	            if(err){
	              return done(err);
	            }

	            if(res.status !== 200) {
	              var _err = JSON.parse(res.text);
	              return done(new Error(_err.message));
	            }

	            var user = res.body;
	            
                expect(user._id).to.be.a('string');
	            expect(user._id).to.equal(userId);
	            expect(user.username).to.equal(userFix.email);


	            done();


			  });
		});		
	});

});

describe('Remove User', function () {
  describe('DELETE /users/:id', function () {
    it('should delete a created user', function (done){
      request(app)
        .delete('/users/' + userId)
        .set('Content-Type', 'application/json')
        .set('Authorization', 'Bearer ' + authToken)
        .end(function (err, res){
            if(err){
              return done(err);
            }

            if(res.status !== 200) {
              var _err = JSON.parse(res.text);
              return done(new Error(_err.message));
            }

            var user = res.body;

                expect(user._id).to.be.a('string');
	            expect(user._id).to.equal(userId);
	            expect(user.username).to.equal(userFix.email);

            done();
          });
    });
  });
});

after(function () {
process.exit(0);
});
