var chai 	= require('chai');
var request = require('supertest');
var app = require('../app');


var userFix = require('./fixtures/user1');
var planFix = require('./fixtures/plan');
var expect = chai.expect;

var userId   = null;
var authToke = '';
var planId   = null;

describe('User Signup', function(){
	describe('POST /users/signup', function(){
		it('should signup a new user', function(done){
			 request(app)
			  .post('/users/signup')
			  .send(userFix)
			  .set('Content-Type', 'application/json')
			  .end(function (err, res){
			  	if(err){
			  		return done(err);
			  	}

			  	if(res.status !== 201){
			  		var _err = JSON.parse(res.text);
			  		return done(new Error(_err.message));
			  	}

			  	var user = res.body;

			  	expect(user._id).to.be.a('string');
			  	expect(user.isActive).to.equal(true);
			  	expect(user.username).to.equal(userFix.email)
			  	userId = user._id;
			  	done();
			  });
		});
	});
});

describe('User Log in', function(){
	var loginInfo = {
		username : userFix.email,
		password : userFix.password
	};

	describe('POST /users/login', function(){
		it('should login a user', function(done){
			request(app)
			  .post('/users/login')
			  .send(loginInfo)
			  .set('Content-Type', 'application/json')
			  .end(function (err, res){
	            if(err){
	              return done(err);
	            }

	            if(res.status !== 200) {
	              var _err = JSON.parse(res.text);
	              return done(new Error(_err.message));
	            }

	            var data = res.body;
	            
	            expect(data.token._id).to.be.a('string');
	            expect(data.token.value).to.be.a('string')
	            expect(data.token.revoked).to.equal(false);

	            authToken = data.token.value;
	            userId = data.token.user;

	            done();


			  });
		});		
	});

});

describe('Create Plan', function(){
	describe('POST /plans/', function(){
		it('should create a new plan', function(done){
			 request(app)
			  .post('/plans/')
			  .send(planFix)
			  .set('Content-Type', 'application/json')
			  .set('Authorization', 'Bearer ' + authToken)
			  .end(function (err, res){
			  	if(err){
			  		return done(err);
			  	}

			  	if(res.status !== 201){
			  		var _err = JSON.parse(res.text);
			  		return done(new Error(_err.message));
			  	}

			  	var plan = res.body;

			  	expect(plan._id).to.be.a('string');
			  	expect(plan.name).to.equal(planFix.name)
			  	expect(plan.description).to.equal(planFix.description)
			  	planId = plan._id;
			  	done();
			  });
		});
	});
});

describe('Get a Single Plan', function(){
	describe('GET /plans/:id',function(){
		it('It should return one single Plan',function(done){
		  request(app)
		    .get('/plans/'+planId)
		    .set('Content-Type', 'application/json')
		    .set('Authorization', 'Bearer ' + authToken)
		    .end(function(err, res){
			    if(err){
	              return done(err);
	            }

	            if(res.status !== 200) {
	              var _err = JSON.parse(res.text);
	              return done(new Error(_err.message));
	            }

	            var plan = res.body;

	            expect(plan._id).to.be.a('string');
	            expect(plan._id).to.equal(planId);
	            expect(plan.description).to.equal(planFix.description);

	            done();
		    });
		});
	});
});

describe('Get Plan Collection', function(){
	describe('GET /plans/',function(){
		it('It should return collection of plans by pagiantion',function(done){
		  request(app)
		    .get('/plans/')
		    .set('Content-Type', 'application/json')
		    .set('Authorization', 'Bearer ' + authToken)
		    .end(function(err, res){
			    if(err){
	              return done(err);
	            }

	            if(res.status !== 200) {
	              var _err = JSON.parse(res.text);
	              return done(new Error(_err.message));
	            }

	            var data = res.body;

	            expect(data.page).to.be.a('number');
	            expect(data.total_docs).to.be.a('number');
	            expect(data.total_pages).to.be.a('number');
	            expect(data.per_page).to.be.a('number');
	            expect(data.page).to.be.a('number');
	            expect(data.docs).to.a('array');

	            done();
		    });
		});
	});
});

describe('Plan Update', function(){
	var updateInfo = {
  		"name":           "Test"
	};

	describe('PUT /plans/:id', function(){
		it('should update a plan', function(done){
			request(app)
			  .put('/plans/'+planId)
			  .send(updateInfo)
			  .set('Content-Type', 'application/json')
			  .set('Authorization', 'Bearer ' + authToken)
			  .end(function (err, res){
	            if(err){
	              return done(err);
	            }

	            if(res.status !== 200) {
	              var _err = JSON.parse(res.text);
	              return done(new Error(_err.message));
	            }

	            var data = res.body;
	            
	            expect(data.name).to.be.a('string');
	            expect(data.description).to.be.a('string');
	            expect(data.name).to.be.equal('Test');
	        


	            done();


			  });
		});		
	});

});

describe('Remove Plan', function () {
  describe('DELETE /plans/:id', function () {
    it('should delete a created plan', function (done){
      request(app)
        .delete('/plans/' + planId)
        .set('Content-Type', 'application/json')
        .set('Authorization', 'Bearer ' + authToken)
        .end(function (err, res){
            if(err){
              return done(err);
            }

            if(res.status !== 200) {
              var _err = JSON.parse(res.text);
              return done(new Error(_err.message));
            }

	            var plan = res.body;

	            expect(plan._id).to.be.a('string');
	            expect(plan._id).to.equal(planId);
	            expect(plan.description).to.equal(planFix.description);

            done();
          });
    });
  });
});


describe('Remove User', function () {
  describe('DELETE /users/:id', function () {
    it('should delete a created user', function (done){
      request(app)
        .delete('/users/' + userId)
        .set('Content-Type', 'application/json')
        .set('Authorization', 'Bearer ' + authToken)
        .end(function (err, res){
            if(err){
              return done(err);
            }

            if(res.status !== 200) {
              var _err = JSON.parse(res.text);
              return done(new Error(_err.message));
            }

            var user = res.body;

                expect(user._id).to.be.a('string');
	            expect(user._id).to.equal(userId);
	            expect(user.username).to.equal(userFix.email);

            done();
          });
    });
  });
});